'use strict';

function checkNorthDoorIsInMap(maze, value) {
    return value >= 0
}

function checkSouthDoorIsInMap(maze, value) {
    return value < maze.size_x * maze.size_y;
}

function checkEastDoorIsInMap(maze, value) {
    return value % maze.size_x != 0 && value < maze.size_x * maze.size_y
}

function checkWestDoorIsInMap(maze, value) {
    return value % maze.size_x !== maze.size_x - 1 && value >= 0
}

function isAlreadyConnectedToMaze(cell1, cell2) {
    return cell1 === cell2
}

function generate(options = { verbose: true }) {
    let numberOfDoor = 0; // The number of door that are openned.
    let z; // Is variable that contain the value of an index

    let rowIndex = 0;
    let colIndex = 0;
    let iteratorValue = 0;
    let cellNumber = 0;


    let print = () => { }
    if (options.verbose) {
        print = (...args) => console.log(...args)
    }



    while (numberOfDoor != this.size_x * this.size_y - 1) {
        rowIndex = this.random.generateNextInt(0, (this.size_x * this.size_y) - 1) // Current cell number looked at.
        colIndex = this.random.generateNextInt(1, 4) // Generate an integer number from 1 to 4 to represent the direction.

        const algorithm = (internalCellNumber) => {
            z = this.indexes[rowIndex];

            for (iteratorValue = 0; iteratorValue < this.indexes.length; iteratorValue++) {
                if (this.indexes[iteratorValue] == z) {
                    this.indexes[iteratorValue] = this.indexes[internalCellNumber];
                }
            }

            this.indexes[rowIndex] = this.indexes[internalCellNumber]; // Connect the cell to the maze

            this.cells[rowIndex * 5 + colIndex] = 1;
        }

        switch (colIndex) {
            case 1:
                cellNumber = rowIndex - this.size_x; // Cell on top of it

                if (checkNorthDoorIsInMap(this, cellNumber)) {

                    if (!isAlreadyConnectedToMaze(this.indexes[rowIndex], this.indexes[cellNumber])) {
                        algorithm(cellNumber)
                        this.cells[(cellNumber) * 5 + (colIndex + 1)] = 1;
                        print("North Door Opened")
                        numberOfDoor++;
                    }

                };
                break;
            case 2:
                cellNumber = rowIndex + this.size_x; // Cell on the bottom of it

                if (checkSouthDoorIsInMap(this, cellNumber)) {
                    if (!isAlreadyConnectedToMaze(this.indexes[rowIndex], this.indexes[cellNumber])) {
                        algorithm(cellNumber)
                        this.cells[(cellNumber) * 5 + (colIndex - 1)] = 1;
                        print("South Door Opened")
                        numberOfDoor++;
                    }

                }
                break;
            case 3:
                cellNumber = rowIndex + 1; // Cell on the left of it
                if (checkEastDoorIsInMap(this, cellNumber)) {
                    if (!isAlreadyConnectedToMaze(this.indexes[rowIndex], this.indexes[cellNumber])) {
                        algorithm(cellNumber)
                        this.cells[(cellNumber) * 5 + (colIndex + 1)] = 1;
                        print("East Door Opened")
                        numberOfDoor++;
                    }
                }
                break;
            case 4:
                cellNumber = rowIndex - 1; // Cell on the right of it
                if (checkWestDoorIsInMap(this, cellNumber)) {
                    if (!isAlreadyConnectedToMaze(this.indexes[rowIndex], this.indexes[cellNumber])) {
                        algorithm(cellNumber)
                        this.cells[(cellNumber) * 5 + (colIndex - 1)] = 1;
                        print("West Door Opened")
                        numberOfDoor++;
                    }
                }
                break;
        }

    }
}
function genereateBitMap() {
    let mazeTxt = " ".repeat(2 * this.size_y + 1).split("");
    let conteur = 0;
    for (let j = 0; j < this.size_x * this.size_y; j = j + this.size_x) {

        for (let i = 0; i < this.size_x; i++) {
            if (this.cells[((i + j) * 5) + 1] == 0) {
                mazeTxt[conteur] = mazeTxt[conteur] + "11";
            } else {
                mazeTxt[conteur] = mazeTxt[conteur] + "10";
            }
            if (i == this.size_x - 1) {
                mazeTxt[conteur] = mazeTxt[conteur] + "1";
            }


        }
        conteur++;

        for (let i = 0; i < this.size_x; i++) {

            if (this.cells[((i + j) * 5) + 4] == 0) {
                mazeTxt[conteur] = mazeTxt[conteur] + "1";
            } else {
                mazeTxt[conteur] = mazeTxt[conteur] + "0";
            }
            if (i == 0 && j == 0) {
                mazeTxt[conteur] = mazeTxt[conteur] + "D";
            } else if (i == this.size_x - 1 && j == this.size_x * this.size_y - this.size_x) {
                mazeTxt[conteur] = mazeTxt[conteur] + "A";
            } else {
                mazeTxt[conteur] = mazeTxt[conteur] + "0";
            }
            if (i == this.size_x - 1) {
                mazeTxt[conteur] = mazeTxt[conteur] + "1";
            }


        }
        conteur++;
        if (j == (this.size_y - 1) * this.size_x) {
            for (let i = 0; i < this.size_x; i++) {
                if (this.cells[((i + j) * 5) + 2] == 0) {
                    mazeTxt[conteur] = mazeTxt[conteur] + "11";
                } else {
                    mazeTxt[conteur] = mazeTxt[conteur] + "10";
                }
                if (i == this.size_x - 1) {
                    mazeTxt[conteur] = mazeTxt[conteur] + "1";
                }


            }
            conteur++;
        }
    }
    return mazeTxt.map(e => e.split(" ")[1]).map(e => e.split(''))
}

function print() {
    let mazeTxt = " ".repeat(2 * this.size_y + 1).split("");
    let conteur = 0;
    for (let j = 0; j < this.size_x * this.size_y; j = j + this.size_x) {

        for (let i = 0; i < this.size_x; i++) {
            if (this.cells[((i + j) * 5) + 1] == 0) {
                mazeTxt[conteur] = mazeTxt[conteur] + " + -";
            } else {
                mazeTxt[conteur] = mazeTxt[conteur] + " +  ";
            }
            if (i == this.size_x - 1) {
                mazeTxt[conteur] = mazeTxt[conteur] + " |";
            }


        }
        conteur++;

        for (let i = 0; i < this.size_x; i++) {

            if (this.cells[((i + j) * 5) + 4] == 0) {
                mazeTxt[conteur] = mazeTxt[conteur] + " | ";
            } else {
                mazeTxt[conteur] = mazeTxt[conteur] + "   ";
            }
            if (i == 0 && j == 0) {
                mazeTxt[conteur] = mazeTxt[conteur] + "D";
            } else if (i == this.size_x - 1 && j == this.size_x * this.size_y - this.size_x) {
                mazeTxt[conteur] = mazeTxt[conteur] + "A";
            } else {
                mazeTxt[conteur] = mazeTxt[conteur] + " ";
            }
            if (i == this.size_x - 1) {
                mazeTxt[conteur] = mazeTxt[conteur] + " |";
            }


        }
        conteur++;
        if (j == (this.size_y - 1) * this.size_x) {
            for (let i = 0; i < this.size_x; i++) {
                if (this.cells[((i + j) * 5) + 2] == 0) {
                    mazeTxt[conteur] = mazeTxt[conteur] + " + -";
                } else {
                    mazeTxt[conteur] = mazeTxt[conteur] + " +  ";
                }
                if (i == this.size_x - 1) {
                    mazeTxt[conteur] = mazeTxt[conteur] + " +";
                }


            }
            conteur++;
        }
    }
    console.log(mazeTxt.join("\n"))



}

function newEmptyMaze(x = 10, y = 10, random) {
    return {
        random,
        size_x: x,
        size_y: y,
        indexes: new Array(x * y).fill(0).map((element, index) => {
            return index;
        }),
        cells: new Array(x * y * 5).fill(0),
        generate,
        print,
        genereateBitMap,
    }
}


module.exports = {
    newEmptyMaze,
}