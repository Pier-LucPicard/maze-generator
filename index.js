const Jimp = require('jimp');
const maze = require('./src/maze');
const random = require('./src/utils/random')

const newMaze = maze.newEmptyMaze(100,100,random);
newMaze.generate();
//newMaze.print();
const map = newMaze.genereateBitMap()


new Jimp(map[0].length,map.length, 0xff0000ff, async (err, image) => {

    for (let x = 0; x < map.length; x++) {
        for (let y = 0; y < map[x].length; y++) {
        switch(map[x][y]){
            case '1': 
                await image.setPixelColour(0x000000ff, y, x)
                break;
            case 'D':
                await image.setPixelColour(0x0000ffff, y, x)
                break;
            case 'A':
                await image.setPixelColour(0xff0000ff, y, x)
                break;
            case '0': 
                await image.setPixelColour(0xffffffff, y, x)
            default: 
                
                break;
        }
    }
        
    }
    console.log('Writting')
    await image.write('test.png')
    // this image is 256 x 256, every pixel is set to 0x00000000
});